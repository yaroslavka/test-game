<?php

namespace common\components;

use yii\base\Behavior;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class SerializeAttributes extends Behavior {

    const TYPE_ARRAY = 1;
    const TYPE_JSON = 2;

    public $attributes;
    public $type = self::TYPE_JSON;

    public function attach($owner) {
        parent::attach($owner);

        $model = $this->owner;
        foreach ($this->attributes as $attrName) {
            $model->$attrName = [];
        }
    }

    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'save',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'save',
            ActiveRecord::EVENT_AFTER_FIND => 'restore',
            ActiveRecord::EVENT_AFTER_INSERT => 'restore',
            ActiveRecord::EVENT_AFTER_UPDATE => 'restore',
        ];
    }

    public function save() {
        $model = $this->owner;
        foreach ($this->attributes as $attrName) {
            $model->$attrName = $this->serialize($model->$attrName);
        }
    }

    public function restore() {
        $model = $this->owner;
        foreach ($this->attributes as $attrName) {
            $model->$attrName = $this->unserialize($model->$attrName);
        }
    }

    protected function serialize($value) {
        if (null === $value || '' === $value) {
            $value = [];
        }
        // maybe we can serialie objects too, but that is not needed right now
        if (!is_array($value)) {
            throw new InvalidParamException('Expecting array');
        }
        switch ($this->type) {
            case static::TYPE_ARRAY:
                return serialize($value);
                break;
            case static::TYPE_JSON:
                return Json::encode($value);
                break;
            default:
                throw new InvalidParamException;
        }
    }

    protected function unserialize($value) {
        if (null === $value || '' === $value) {
            return [];
        }
        if (!is_string($value)) {
            throw new InvalidParamException('Expecting string ' . print_r($value, true));
        }
        switch ($this->type) {
            case static::TYPE_ARRAY:
                return unserialize($value);
                break;
            case static::TYPE_JSON:
                return Json::decode($value);
                break;
            default:
                throw new InvalidParamException;
        }
    }

}
