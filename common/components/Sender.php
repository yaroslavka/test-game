<?php
namespace common\components;

use yii;
use yii\httpclient\Client;


class Sender
{
    public function post($url, $data)
    {
        $client = new Client();
        $request = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addData(['data' => $data]);
        try {
            $response = $request->send();
            return $response->data;
        } catch (\Exception $e) {
            Yii::error($e);
            if (YII_DEBUG) {
                throw $e;
            }
            return false;
        }
    }
}