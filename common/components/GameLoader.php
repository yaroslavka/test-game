<?php

namespace common\components;

class GameLoader
{

    public function loadGame()
    {
        return $this->loadXml();
    }

    /**
     * @param $str
     * @return bool|\SimpleXMLElement
     */
    protected function stringToXml($str) {
        if (!$str) {
            return false;
        }
        $errorMode = libxml_use_internal_errors(true);
        $xml = simplexml_load_string($str);
        $errors = libxml_get_errors();
        libxml_clear_errors();
        libxml_use_internal_errors($errorMode);
        if ($errors) {
            print_r($errors);
            return false;
        }
        if ($resp = $xml->xpath('/error')) {
            print_r(trim((string) $resp[0]));
            return false;
        }
        return $xml;
    }

    /**
     * @param $url
     * @return bool|\SimpleXMLElement
     */
    protected function loadXml() {
        $str = @file_get_contents(\Yii::getAlias('@common/components/xml/game.xml'));
        return $str ? $this->stringToXml($str) : false;
    }

}