<?php
namespace common\components;

class GameManager
{
    public function loadGame()
    {
        $loader = new GameLoader();
        $xml = $loader->loadGame();
        if (!$xml) {
            $this->log("no xml");
        }
        $this->sendToApi($xml);
    }

    public function sendToApi($xml)
    {
        $data = [];
        foreach ((array)$xml->texts as $attributes){
            foreach ($attributes as $attribute => $value){
                $data[$attribute] = $value;
            }
        }
        $sender  = new Sender();
        $response = $sender->post('http://api.lh/api/v1/game',$data);
        $this->log("result: ". $response['data']['result']);
    }

    private function log()
    {

        $addEol = false;
        foreach (func_get_args() as $arg) {
            if (is_string($arg)) {
                echo $arg;
                $addEol = true;
            } else {
                var_dump($arg);
            }
        }
        if ($addEol) {
            echo PHP_EOL;
        }
    }


}
