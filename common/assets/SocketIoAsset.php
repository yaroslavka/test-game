<?php

namespace common\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;

class SocketIoAsset extends AssetBundle
{
    /**
     * Node sender component name (class NodeJsHelpery)
     * @var string
     */
    public $nodejsName = 'nodejs';

    public $sourcePath = '@bower/socket.io-client';

    public $js = [
       'socket.io.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD,
    ];

    /**
     * @var View
     */
    public $view;

    public static function register($view)
    {
        $inst = parent::register($view);
        $inst->view = $view;
        return $inst;
    }

    /**
     * Init socket io and connect to server
     */
    public function connect()
    {
        $nodejs = Yii::$app->get($this->nodejsName);
        if (!$nodejs) {
            return;
        }
        $host = Yii::$app->nodejs->host . ':' . Yii::$app->nodejs->port;
        $js=<<<JS
var socket = io.connect('{$host}');
JS;
        $this->view->registerJs($js, View::POS_BEGIN);
    }
}