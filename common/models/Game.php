<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $request
 * @property integer $status
 * @property string $date_create
 */
class Game extends \yii\db\ActiveRecord
{
    const STATUS_SUCCESS = 0;
    const STATUS_ERROR = 1;
    const STATUS_OTHER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request','status'], 'required'],
            [['status'], 'default','value'=> self::STATUS_SUCCESS],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function status()
    {
        return [
            self::STATUS_SUCCESS => 'Success',
            self::STATUS_ERROR => 'Error',
            self::STATUS_OTHER => 'Other Error',
        ];
    }

    public function saveGame($post){
        if(isset($post['GOAL']) && isset($post['CURRENT']) && isset($post['MAX'])){
            $model = new static;
            $model->request = serialize($post);
            $model->status = 0;
            if(!$model->save()){
                return 2;
            }
            return 0;
        }else{
            $model = new static;
            $model->request = serialize($post);
            $model->status = 1;
            if(!$model->save()){
                return 2;
            }
            return 1;
        }
    }
}
