<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone' => 'America/New_York',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'emailHelper' => [
            'class' => 'common\components\EmailHelper',
            'apiKey' => 'key-80b301ddd11a4497e252f9fe8d655434',
            'domain' => 'sandbox945408c3fd7445fc92c6f21d02fc12d1.mailgun.org',
        ],
        'nodejs' => [
            'class' => 'common\components\NodeJsHelper',
            'host' => DOMAIN_NODEJS,
            'port' => 5858,
        ],[
            'class' => 'common\models\ContestGame',
            'on start' => ['common\components\ContestEventListener', 'start'],
        ],
    ],
    'modules' => [
        'gallery' => [
            'class' => 'dvizh\gallery\Module',
            'imagesStorePath' => dirname(dirname(__DIR__)).'/frontend/web/images/store', //path to origin images
            'imagesCachePath' => dirname(dirname(__DIR__)).'/frontend/web/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD',
            'placeHolderPath' => '@webroot/images/placeHolder.png',
            'adminRoles' => ['Admin'],
        ],
    ],
];
