<?php
require 'defines.php';

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');

//Event::on('common\models\ContestGame', ContestGame::EVENT_TERMINATE, ['common\components\ContestEventListener', 'onTerminate']);
//Event::on('common\models\ContestGame', ContestGame::EVENT_CLOSE, ['common\components\ContestEventListener', 'onClose']);
//Event::on('common\models\ContestGame', ContestGame::EVENT_START, ['common\components\ContestEventListener', 'onStart']);
//Event::on('common\models\ContestGame', ContestGame::EVENT_UPDATE_BET, ['common\components\ContestEventListener', 'onUpdateBet']);
//Event::on('common\models\ContestGame', ContestGame::EVENT_LIVE, ['common\components\ContestEventListener', 'onUpdateLive']);
//Event::on('common\models\ContestGame', ContestGame::EVENT_FINAL, ['common\components\ContestEventListener', 'onUpdateFinal']);
