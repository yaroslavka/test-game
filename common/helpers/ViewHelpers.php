<?php

namespace common\helpers;

use Yii;

class ViewHelpers
{
	public static function superAdminDeleteAttension()
	{
		return 'Attension! You use Super Administrator account. Deleting action cannot be revert. Are you sure?';
	}
}