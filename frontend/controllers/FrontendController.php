<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\Response;
use common\models\User;

class FrontendController extends Controller {

    /**
     * @var \common\models\User
     */
    public $adminModel = null;
    public $siteTitle = null;
    /**
     * @var \common\models\User
     */
    public $userModel = null;

    public $isAjaxForm = false;

    public $layout = 'main';

    public $enableCsrfValidation = false;

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action))
        {
            return false;
        }
        return true;
    }

    
    public function ajaxAction()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException('This url can only be accesed by ajax');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}
