var index = (function () {
    var $body = $('body');
   
    var ajaxWindow = function(){
        $body.on('click','.ajax-window',function(){
            var self = $(this);
            $.ajax({
                url: self.attr('href'),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if(data.content){
                        $('.ajax-modal-content').append(data.content);
                    }
                }
            });
            return false;
        });
    }
    
    var ajaxWindowSubmit = function(){
        $body.on('click','.ajax-window-submit',function(){
            var self = $(this);
            $.ajax({
                url: self.attr('href'),
                type: 'POST',
                dataType: 'json',
                data: self.parents('form').serialize(),
                success: function (data) {
                    if(data.content){
                        $('.ajax-modal-content').append(data.content);
                    }
                }
            });
            return false;
        });
    }
    
    var insertInInput = function(){
        $body.on('keypress, keyup, keydown','.form-control',function(){
            $(this).parents('.has-error').find('.help-block-error').remove();
            $(this).parents('.has-error').removeClass('has-error');
        });
    }
    
    
    var close = function(){
        $body.on('click','.close-modal',function(){
            $('.ajax-modal-content').html('');
            return false;
        });
    }
    
    var initCarousel = function() {
	$('div.scoreboard').scrollGallery({
		mask: 'div.mask',
		slider: '.slideset',
		slides: '.slide',
		currentNumber: 'span.cur-num',
		totalNumber: 'span.all-num',
		disableWhileAnimating: true,
		generatePagination: 'div.pagination',
		circularRotation: false,
		pauseOnHover: false,
		autoRotation: false,
		maskAutoSize: false,
		switchTime: 2000,
		animSpeed: 600
	});

    }
    
    var init = function () {
        ajaxWindow();
        ajaxWindowSubmit();
        insertInInput();
        close();
        initCarousel();
    }
    
    

    return {
        init: init,
    }

}());

(function () {
    index.init();
}());

