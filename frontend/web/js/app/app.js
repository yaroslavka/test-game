﻿var app = angular.module('app', [
    'controllers',
    'directives',
    'services',
    'angular-growl',
    'ngDialog',
    'ngImgCrop',
    'angularFileUpload'
]);

app.config(['$httpProvider', 'growlProvider', 'ngDialogProvider',
    function ($httpProvider, growlProvider, ngDialogProvider) {

        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        growlProvider.globalTimeToLive(5000);
        growlProvider.globalDisableCountDown(true);

        ngDialogProvider.setDefaults({
            closeByNavigation: true
        });
    }
]);
