services.factory('EmailForm', ['$resource', function ($resource) {
    return $resource(inlineVars['email-form']);
}]);

services.factory('PasswordForm', ['$resource', function ($resource) {
    return $resource(inlineVars['password-form']);
}]);