controllers.controller('AccountController', ['$scope', '$rootScope','ngDialog',
    function ($scope, $rootScope, ngDialog) {

        $scope.changeEmail = function(){
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=email",
                    controller: 'EmailController',
                    scope: $scope
                }
            );
        }

        $scope.changePassword = function(){
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=password",
                    controller: 'PasswordController',
                    scope: $scope
                }
            );
        }
        
        $scope.changeLogo = function(){
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=logo",
                    controller: 'LogoController',
                    scope: $scope
                }
            );
        }
        
        $scope.getPairs = function (id) {
            $scope.pairs_id = id;
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=pairs_created",
                    controller: 'PairsController',
                    scope: $scope
                }
            );
        }
        
        $rootScope.$watch('user_email', function () {
            $scope.user_email = $rootScope.user_email ? $rootScope.user_email : $scope.user_email;
        });
        
        $rootScope.$watch('user_logo', function () {
            $scope.user_logo = $rootScope.user_logo ? $rootScope.user_logo: $scope.user_logo;
        });
        
    }
]);

controllers.controller('EmailController', ['$scope','$rootScope', 'ngDialog','EmailForm','growl',
    function ($scope, $rootScope, ngDialog, EmailForm, growl) {
        
        $scope.emailForm ={};
        
        $scope.email = function(){
            var form = new EmailForm();
            form.EmailForm = $scope.emailForm;
            form.$save(function(response){
                $scope.emailForm.$error =[];
                if(response.error){
                    $scope.emailForm.$error = response.error;
                }else if(response.success){
                    $rootScope.user_email = $scope.emailForm.email; 
                    ngDialog.closeAll();
                    growl.success("SUCCESS");
                }
            });
        }
        
        $scope.verify = function(name){
            if($scope.emailForm.$error && $scope.emailForm.$error[name]){
                $scope.emailForm.$error[name]='';
            }
        }
    }
]);


controllers.controller('PasswordController', ['$scope','ngDialog','PasswordForm', 'growl',
    function ($scope, ngDialog, PasswordForm , growl) {
        
        $scope.passwordForm ={};
        
        $scope.password = function(){
            var form = new PasswordForm();
            form.PasswordForm = $scope.passwordForm;
            form.$save(function(response){
                $scope.passwordForm.$error =[];
                if(response.error){
                    $scope.passwordForm.$error = response.error;
                }else if(response.success){
                    ngDialog.closeAll();
                    growl.success("SUCCESS");
                }
            });
        }
        
        $scope.verify = function(name){
            if($scope.passwordForm.$error && $scope.passwordForm.$error[name]){
                $scope.passwordForm.$error[name]='';
            }
        }
    }
]);


controllers.controller('LogoController', ['$scope', '$rootScope', '$timeout','ngDialog', 'growl','FileUploader',
    function ($scope, $rootScope, $timeout, ngDialog , growl, FileUploader) {
        $scope.myImage='';
        $scope.myCroppedImage='';
        $scope.imageError='';
        
        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            if(file.type != 'image/jpeg' && file.type != 'image/png' && file.type != 'image/jpg'){
                    $scope.$apply(function ($scope) {
                    $scope.myImage = '';
                    $scope.myCroppedImage='';
                    file = null;
                    $scope.imageError = 'Only jpg ,jpeg ,png formats';
                });
                return false;
            }
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.imageError = '';
                    $scope.myImage = evt.target.result;
                });
            };
            $scope.myImageCheckbox = false;
            reader.readAsDataURL(file);
        };
        
        var dataURItoBlob = function(dataURI) {
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for(var i = 0; i < binary.length; i++) {
              array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {type: mimeString});
        };
        
        var uploader = $scope.uploader = new FileUploader({
            url: inlineVars['image-form'],
            autoUpload: false
        });
        
        $scope.showImage = function(){
            uploader.addToQueue(dataURItoBlob($scope.myCroppedImage));
            uploader.uploadAll();
            uploader.onSuccessItem = function(item, response, status, headers) {
                if(response.success){
                    $rootScope.user_logo = response.logo;
                    ngDialog.closeAll();
                    growl.success("SUCCESS");
                }
            };
        }
        
        $timeout(function(){
            angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
        }, 1000);
        
        
    }
]);