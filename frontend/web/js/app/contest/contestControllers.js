controllers.controller('ContestController', ['$scope', '$rootScope', 'ngDialog', 'GetContest', 'GetHeroes',
    function ($scope, $rootScope, ngDialog, GetContest, GetHeroes) {
        
        $scope.totalPickHeroes = 5
        $scope.contests = {};
        $scope.pickHeroes = {};
        $scope.heroes = {};
       
        $scope.sortBy = function(propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? ! $scope.reverse : false;
            $scope.propertyName = propertyName;
        };
        
        $scope.loadContest = function() {
            GetContest.get(function (response) {
                if (response.data) {
                    $scope.contests = response.data;
                }
            });
        } 
        
        $scope.loadHeroes = function() {
            GetHeroes.get(function (response) {
                if (response.data) {
                    $scope.heroes = response.data;
                }
            });
        }
        
        $scope.deleteHero = function($id){
            var __hero = $scope.pickHeroes[$id];
            $scope.error_message = null;
            $scope.points = $scope.points + __hero.point;
            $scope.heroes[__hero.skill][__hero.id].check = true;
            delete $scope.pickHeroes[$id];
        }
        
        $scope.chooseHero = function($skill, $id){
            var __hero = $scope.heroes[$skill][$id];
            $scope.error_message = null;
            if($scope.points < __hero.point){
                $scope.error = true;
                $scope.error_message = 'You have not enough points.';
                return false;
            }  
            if(__hero.check == false){
                $scope.error = true;
                $scope.error_message = 'All heroes should be unique.';
                return false;
            }
            
            $scope.points = $scope.points - __hero.point;
            for(var i = 1 ;i <= $scope.totalPickHeroes; i++ ){
                if($scope.pickHeroes[i] == undefined){
                    $scope.pickHeroes[i] = __hero;
                    __hero.check = false;
                    break;
                }
            }
        }
        $scope.loadContest();
    }
]);

controllers.controller('ContestEnterController', ['$scope', '$controller','$rootScope', '$timeout', 'ngDialog', 'BuyExtraPoints','SaveContest',
    function ($scope, $controller, $rootScope, $timeout, ngDialog, BuyExtraPoints, SaveContest) {
        
        angular.extend(this, $controller('ContestController', {$scope: $scope}));
        
        $scope.id;
        $scope.error = false;
        $scope.pickError = false;
        $scope.error_message = '';
        $scope.extraPoint = 3;
        $scope.points = 15;
        $scope.lineup = 0;
       
        $scope.popupExtraPoint = function(){
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=extra_point",
                    scope: $scope
                }
            );
        }
        
        $scope.buyExtraPoints = function(){
            var extraPoints = new BuyExtraPoints();
                extraPoints.extraPoint = $scope.extraPoint;
                extraPoints.contest_id = $scope.id;
                extraPoints.lineup = $scope.lineup;
                extraPoints.$save(function (response) {
                    if(response.success){
                        $scope.points = $scope.points + $scope.extraPoint;
                        ngDialog.closeAll();
                    }else{
                        
                    }
                });
        }
        
        $scope.save = function(){
            $scope.error_message = null;
            var saveForm = new SaveContest();
            saveForm.heroes = $scope.pickHeroes;
            saveForm.contest_id = $scope.id;
            saveForm.lineup = $scope.lineup;
            saveForm.$save(function (response) {
                if(response.error){
                    angular.forEach(response.error, function(message, key) {
                        $scope.error_message = message[0];
                    });
                }else{
//                    ngDialog.open({
//                        templateUrl: "/ajax/get-template?name=success_contest",
//                        scope: $scope
//                    });
//                    $timeout(function () {
//                        window.location = response.redirect;
//                    }, 3000);
                }
            });
        }
        
        $scope.loadHeroes();
        
    }
]);


controllers.controller('ContestUpdateController', ['$scope', '$controller','GetUserLineup','UpdateContest',
    function ($scope, $controller, GetUserLineup, UpdateContest) {
        
        angular.extend(this, $controller('ContestEnterController', {$scope: $scope}));
        
        $scope.getUserLineup = function(){
            GetUserLineup.get({contest_id:$scope.id, lineup:$scope.lineup}, 
            function (response) {
                if (response.data) {
                    $scope.points = response.data.points
                }
                if(response.lineup){
                    angular.forEach(response.lineup, function(data, key) {
                        $scope.chooseHero(data.skill,data.id);
                    });
                }
            });
        }
        
        $scope.$watch('lineup', function () {
            $scope.getUserLineup();
        });
    }

]);
    
    
    


