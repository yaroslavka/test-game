services.factory('GetContest', ['$resource', function ($resource) {
    return $resource(inlineVars['get-contests']
    );
}]);

services.factory('GetHeroes', ['$resource', function ($resource) {
    return $resource(inlineVars['get-heroes']
    );
}]);

services.factory('GetUserLineup', ['$resource', function ($resource) {
    return $resource(inlineVars['get-user-lineup'],
        {
            contest_id:'@_contest_id',
            lineup:'@_lineup',
        }
    );
}]);

services.factory('BuyExtraPoints', ['$resource', function ($resource) {
    return $resource(inlineVars['buy-extra-points']);
}]);


services.factory('SaveContest', ['$resource', function ($resource) {
    return $resource(inlineVars['save-contest']);
}]);

services.factory('UpdateContest', ['$resource', function ($resource) {
    return $resource(inlineVars['update-contest']);
}]);



//services.factory('Socket', function () {
//    var socket = io.connect(inlineVars['host-name']+':5858');
//    return socket;
//});
