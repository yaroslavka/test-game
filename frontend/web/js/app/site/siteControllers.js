var controllers = angular.module('controllers', []);

controllers.controller('SiteController', ['$rootScope', '$scope', 'ngDialog',
    function ($rootScope, $scope, ngDialog) {

        $scope.login = function () {
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=login",
                    controller: 'LoginController',
                    scope: $scope
                }
            );
        };

        $scope.signup = function () {
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=signup",
                    controller: 'SignupController',
                    scope: $scope
                }
            );
        };

        $scope.signupFinish = function () {
            ngDialog.open({
                    templateUrl: "/ajax/get-template?name=signup_finish",
                    controller: 'SignupController',
                    scope: $scope,
                    closeByEscape: false,
                    showClose: false,
                    closeByNavigation: false
                }
            );
        };

        $scope.init = function (data) {
            if (data !== undefined) {
                $scope.userForm = {};
                $.each(data, function (index, value) {
                    $scope.userForm[index] = value;
                });
                $scope.signupFinish();
            }
        };
    }
]);

controllers.controller('LoginController', ['$scope', '$timeout', '$window', 'ngDialog', 'LoginForm', 'ForgotForm',
    function ($scope, $timeout, $window, ngDialog, LoginForm, ForgotForm) {

        $scope.loginForm = {};
        $scope.forgotForm = {};
        $scope.forgotMessage = '';
        $scope.loginBlock = true;
        $scope.forgotBlock = false;

        $scope.login = function () {
            var form = new LoginForm();
            form.LoginForm = $scope.loginForm;
            form.$save(function (response) {
                $scope.loginForm.$error = [];
                if (response.error) {
                    $scope.loginForm.$error = response.error;
                } else if (response.success) {
                    $window.location.href = '/';
                }
            });
        };

        $scope.forgot = function () {
            var form = new ForgotForm();
            form.ForgotForm = $scope.forgotForm;
            form.$save(function (response) {
                $scope.forgotForm.$error = [];
                if (response.error) {
                    $scope.forgotForm.$error = response.error;
                } else if (response.success) {
                    $scope.forgotMessage = response.message;
                    $timeout(function () {
                        ngDialog.closeAll()
                    }, 5000);
                }
            });
        };

        $scope.verify = function (name) {
            if ($scope.loginForm.$error && $scope.loginForm.$error[name]) {
                $scope.loginForm.$error[name] = '';
            }
        };

        $scope.verifyF = function (name) {
            if ($scope.forgotForm.$error && $scope.forgotForm.$error[name]) {
                $scope.forgotForm.$error[name] = '';
            }
        };

        $scope.changeBlock = function () {
            if ($scope.loginBlock) {
                $scope.loginBlock = false;
                $scope.forgotBlock = true;
            } else {
                $scope.loginBlock = true;
                $scope.forgotBlock = false;
            }
        }

    }
]);

controllers.controller('SignupController', ['$scope', '$timeout', 'ngDialog', 'SignupForm', 'UserForm',
    function ($scope, $timeout, ngDialog, SignupForm, UserForm) {

        $scope.signupForm = {};

        $scope.successMessage = '';

        $scope.signup = function () {
            var form = new SignupForm();
            form.SignupForm = $scope.signupForm;
            form.$save(function (response) {
                $scope.signupForm.$error = [];
                if (response.error) {
                    $scope.signupForm.$error = response.error;
                } else if (response.success) {
                    $scope.successMessage = response.message;
                    $timeout(function () {
                        ngDialog.closeAll()
                    }, 5000);
                }
            });
        };

        $scope.finish = function () {
            var form = new UserForm();
            form.User = $scope.userForm;
            form.$save(function (response) {
                $scope.userForm.$error = [];
                if (response.error) {
                    $scope.userForm.$error = response.error;
                } else if (response.success) {
                    $scope.successMessage = response.message;
                    $timeout(function () {
                        ngDialog.closeAll()
                    }, 5000);
                }
            });
        };

        $scope.verify = function (name) {
            if ($scope.signupForm.$error && $scope.signupForm.$error[name]) {
                $scope.signupForm.$error[name] = '';
            }
        };
    }
]);
