var directives = angular.module('directives', []);

directives.directive('dynamic', function ($compile) {
    return {
        restrict: 'A',
        replace: true,
        link: function (scope, ele, attrs) {
            scope.$watch(attrs.dynamic, function (html) {
                if (html == undefined) {
                    return false;
                }
                ele.html(html.$$unwrapTrustedValue());
                $compile(ele.contents())(scope);
            });
        }
    };
});


directives.directive('countdown', [
    'Util',
    '$interval',
    function (Util, $interval) {
        return {
            restrict: 'A',
            scope: {date: '@'},
            link: function (scope, element) {
                var future;
                future = new Date(scope.date);
                $interval(function () {
                    var diff;
                    diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
                    return element.text(Util.dhms(diff));
                }, 1000);
            }
        };
    }
]).factory('Util', [function () {
        return {
            dhms: function (t) {
                var days, hours, minutes, seconds;
                days = Math.floor(t / 86400);
                t -= days * 86400;
                hours = Math.floor(t / 3600) % 24;
                t -= hours * 3600;
                minutes = Math.floor(t / 60) % 60;
                t -= minutes * 60;
                seconds = t % 60;
                if(days > 0){
                   return [days + ' days'].join(' ');
                }else{
                    return [
                        hours + ' :',
                        minutes + ' :',
                        seconds 
                    ].join(' ');
                }
                
            }
        };
    }]);

