var services = angular.module('services', ['ngResource']);

services.factory('LoginForm', ['$resource', function ($resource) {
    return $resource(inlineVars['login-form']);
}]);

services.factory('SignupForm', ['$resource', function ($resource) {
    return $resource(inlineVars['signup-form']);
}]);

services.factory('UserForm', ['$resource', function ($resource) {
    return $resource(inlineVars['user-form']);
}]);

services.factory('ForgotForm', ['$resource', function ($resource) {
    return $resource(inlineVars['forgot-form']);
}]);

