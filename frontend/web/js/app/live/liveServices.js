services.factory('PairsData', ['$resource', function ($resource) {
    return $resource(inlineVars['pairs-data']);
}]);

services.factory('PairData', ['$resource', function ($resource) {
    return $resource(inlineVars['pair-data']);
}]);
