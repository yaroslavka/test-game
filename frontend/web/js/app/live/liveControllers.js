controllers.controller('LiveController', ['$scope', 'ngDialog','Socket', 'PairsData', '$timeout',
    function ($scope, ngDialog, Socket, PairsData, $timeout) {
        $scope.lobbyList = true;
        $scope.statics = [];
        $scope.button = [];
        $scope.data = [];
        $scope.test = false;


        $scope.showStatics = function (id) {
            $scope.statics[id] = $scope.statics[id] ? false : true;
            $scope.button[id] = $scope.button[id] == 'show' ? 'hide' : 'show';
        }

        $scope.$watch('contest_id', function () {
            if ($scope.contest_id) {
                PairsData.get({id: $scope.contest_id}, function (response) {
                    if (response.data) {
                        $scope.data = response.data;
                        console.log($scope.data);
                    }
                });
            }

        });

        Socket.on('message', function (data) {
            $scope.test = true;
            data = JSON.parse(data);
            if (data.message === 'contest-live' && $scope.contest_id === data.id) {
                PairsData.get({id: data.id}, function (response) {
                    if (response.data) {
                        angular.forEach($scope.data, function (value, key) {
                            if (response.data[key]) {
                                angular.forEach($scope.data[key].PlayerOne, function (val, k1) {
                                    if (k1.indexOf('_live') === -1) {
                                        if ($scope.data[key].PlayerOne[k1] !== response.data[key].PlayerOne[k1]) {
                                            var method = k1 + '_live';
                                            $scope.data[key].PlayerOne[method] = true;
                                            $scope.data[key].PlayerOne[k1] = response.data[key].PlayerOne[k1];
                                        }
                                    }
                                });
                                angular.forEach($scope.data[key].PlayerSecond, function (val, k2) {
                                    if (k2.indexOf('_live') === -1) {
                                        if ($scope.data[key].PlayerSecond[k2] !== response.data[key].PlayerSecond[k2]) {
                                            var method = k2 + '_live';
                                            $scope.data[key].PlayerSecond[method] = true;
                                            $scope.data[key].PlayerSecond[k2] = response.data[key].PlayerSecond[k2];
                                        }
                                    }
                                });
                                
                            }
                        });
                        $timeout(function () {
                            $scope.data= response.data; 
                        }, 5000);
                        
                    }
                });
            }
            
            if(data.message === 'contest-final' && $scope.contest_id === data.id){
                ngDialog.open({
                    template: "/ajax/get-template?name=final_contest",
                    controller: 'FinalController',
                    scope: $scope
                });
            }
            
            
        });
    }
]);

controllers.controller('FinalController', ['$scope', '$window',
    function ($scope, $window) {
        $scope.final = function(){
            $window.location.href = '/';
        }
    }
]);