controllers.controller('HistoryController', ['$scope',  'ngDialog', 
    function ($scope, ngDialog) {
       $scope.getDetails = function (id) {
            $scope.deteils_id = id;
            ngDialog.open({
                    template: "/ajax/get-template?name=pairs_history",
                    controller: 'DetailsController',
                    scope: $scope
                }
            );
        }
    }
]);

controllers.controller('DetailsController', ['$scope', 'GetDetails',
    function ($scope, GetDetails) {
       
        $scope.statics = false;
        $scope.button = 'show';
        $scope.data = {};
       
        GetDetails.get({id: $scope.deteils_id}, function (response) {
            if (response.data) {
                $scope.data = response.data;
            }
        });

        $scope.showStatics = function () {
            $scope.statics = $scope.statics ? false : true;
            $scope.button = $scope.statics ? 'hide' : 'show';
        }
    }
]);