<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="app">
<head>
    <title>test</title>
</head>
<body class="home-page">
<?php $this->beginBody() ?>
<div id="wrapper">
    <?= $content; ?>
</div>
<?php $this->endBody() ?>
</body>
</html>

