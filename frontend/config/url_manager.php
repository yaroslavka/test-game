<?php

/*
 * this config can be used in backend
 */

return [
    'class' => 'yii\web\UrlManager',
    'baseUrl' => '',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        '/' => 'site/index',
        '/login' => 'site/login',
        '/logout' => 'site/logout',
        '/signup' => 'site/signup',
        '/forgot' => 'site/forgot',


        '<controller:\w+>s' => '<controller>/index',
        '<controller:\w+>/update/<id:\d+>' => '<controller>/update',
        '<controller:\w+>/delete/<id:\d+>' => '<controller>/delete',
        '<controller:\w+>/<id:\d+>' => '<controller>/view',
    ],
];
