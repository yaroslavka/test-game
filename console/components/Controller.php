<?php

namespace console\components;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;

class Controller extends \yii\console\Controller
{

    protected static $sleep = 5000;

    public function init()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(-1);
        set_time_limit(0);
        parent::init();
    }


    public function extension()
    {
        return [
            'large' => [
                'width' =>  200,
                'height' => 200,
            ]
        ];
    }

    public function extensionLong()
    {
        return [
            'large' => [
                'width' =>  150,
                'height' => 450,
            ]
        ];
    }

    public function extensionWidth()
    {
        return [
            'large' => [
                'width' =>  300,
                'height' => 100,
            ]
        ];
    }

    public function extensionSave($model, $dir)
    {
        foreach ($this->extension() as $resizeDir => $resize){
            $resizeDir =  $dir . DIRECTORY_SEPARATOR . $resizeDir;
            if (!is_dir($resizeDir)){
                mkdir($resizeDir, 0755, true);
            }
            Image::frame($dir . DIRECTORY_SEPARATOR . $model->image)
                ->resize(new Box($resize['width'], $resize['height']))
                ->save($resizeDir . DIRECTORY_SEPARATOR . $model->image ,['flatten' => false, 'quality' => 100]);
        }
    }

    public function extensionWidhtSave($model, $dir)
    {
        foreach ($this->extensionWidth() as $resizeDir => $resize){
            $resizeDir =  $dir . DIRECTORY_SEPARATOR . $resizeDir;
            if (!is_dir($resizeDir)){
                mkdir($resizeDir, 0755, true);
            }
            Image::frame($dir . DIRECTORY_SEPARATOR . $model->image)
                ->resize(new Box($resize['width'], $resize['height']))
                ->save($resizeDir . DIRECTORY_SEPARATOR . $model->image ,['flatten' => false, 'quality' => 100]);
        }
    }

    public function extensionLongSave($model, $dir)
    {
        foreach ($this->extensionLong() as $resizeDir => $resize){
            $resizeDir =  $dir . DIRECTORY_SEPARATOR . $resizeDir;
            if (!is_dir($resizeDir)){
                mkdir($resizeDir, 0755, true);
            }
            Image::frame($dir . DIRECTORY_SEPARATOR . $model->image)
                ->resize(new Box($resize['width'], $resize['height']))
                ->save($resizeDir . DIRECTORY_SEPARATOR . $model->image ,['flatten' => false, 'quality' => 100]);
        }
    }


    /**
     * Some services have throttle limit
     */
    protected static function sleep()
    {
        usleep(self::$sleep);
    }
}