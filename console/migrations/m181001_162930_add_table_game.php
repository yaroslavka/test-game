<?php

use yii\db\Schema;
use yii\db\Migration;

class m181001_162930_add_table_game extends Migration
{
    public function up()
    {
        $sql=<<<SQL
                DROP TABLE IF EXISTS `game`;
                CREATE TABLE IF NOT EXISTS `game` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `request` TEXT,
                    `status` tinyint(1) NOT NULL DEFAULT 0,
                    `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SQL;
        $this->execute($sql);
    }

    public function down()
    {
        echo "m181001_162930_add_table_game cannot be reverted.\n";

        return false;
    }
}
