<?php
namespace console\controllers;

use console\components\Controller;
use common\components\GameManager;
use common\models\Game;

class GameController extends Controller
{
    protected $dataManager;
    protected static $sleep = 500;

    public function init()
    {
        parent::init();
        $this->dataManager = new GameManager;
    }

    public function actionTruncate()
    {
        $db = \Yii::$app->db;
        $db->createCommand()->truncateTable(Game::tableName())->execute();
    }

    /**
     * Load Game
     * @param null $league
     */
    public function actionIndex()
    {
        echo 'loading game', PHP_EOL;
        $this->dataManager->loadGame();
    }

    /**
     * Some services have throttle limit
     */
    protected static function sleep()
    {
        usleep(self::$sleep);
    }
}