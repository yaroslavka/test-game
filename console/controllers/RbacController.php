<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        // add "User" role and give this role the "createPost" permission
        $user = $auth->createRole('User');
        $auth->add($user);

        // add "Manager" role and give this role the "updatePost" permission
        // as well as the permissions of the "User" role
//        $manager = $auth->createRole('Property Manager');
//        $auth->add($manager);
        
        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        // add "Manager" role and give this role the "updatePost" permission
        // as well as the permissions of the "User" role
//        $wch = $auth->createRole('WCH User');
//        $auth->add($wch);
//        $auth->addChild($wch, $manager);
//        $auth->addChild($wch, $user);
        
        
        $admin = $auth->createRole('Admin');
        $auth->add($admin);
        $auth->addChild($admin, $user);
        
        
        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($admin, 1);
    }
}