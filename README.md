NODEJS COMMANDS
-------------------
```
ps aux | grep node                                         show ID's nodeJS server

kill -9 ID                                                 off nodeJS server by ID 

sudo iptables -I INPUT -p tcp --dport 5858 -j ACCEPT       open port by centos

node server.js                                             inclusion nodeJS server

forever start server.js                                    inclusion nodeJS server through forever tools

forever stop server.js                                     off nodeJS server through forever tools
```


1. composer global require "fxp/composer-asset-plugin:~1.1.1" (console command)    
    1.1 composer create-project --prefer-dist yiisoft/yii2-app-advanced yii-application (console command)    

2. php init (console command)
    2.1 development
    2.2 yes

3. composer update (console command)

4. add to common/config/main-local.php => 
        components =>[
            'authManager' => [
                'class' => 'yii\rbac\DbManager',
        ],
    ]

5. php yii migrate --migrationPath=@yii/rbac/migrations/ (console command)  

6. php yii rbac/init (console command)

7. php yii migrate (console command)

7.2 php yii migrate/up --migrationPath=@vendor/dvizh/yii2-gallery/src/migrations
       for gallery (tables)
       
7.3 add to common/config/main.php 
                    'modules' => [
                       'gallery' => [
                           'class' => 'dvizh\gallery\Module',
                           'imagesStorePath' => dirname(dirname(__DIR__)).'/frontend/web/images/store', //path to origin images
                           'imagesCachePath' => dirname(dirname(__DIR__)).'/frontend/web/images/cache', //path to resized copies
                           'graphicsLibrary' => 'GD',
                           'placeHolderPath' => '@webroot/images/placeHolder.png',
                           'adminRoles' => ['	Admin'],
                       ],
                   ],