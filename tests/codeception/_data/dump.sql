-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';


DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `placeholders` text NOT NULL,
  `subject` varchar(1000) NOT NULL,
  `body` text NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `date_updated` (`date_updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `game_player_stats`;
CREATE TABLE `game_player_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feed_id` int(10) unsigned NOT NULL,
  `player_id` int(10) unsigned NOT NULL,
  `position` varchar(10) NOT NULL DEFAULT '',
  `minutes` int(10) unsigned NOT NULL DEFAULT '0',
  `field_goals_made` int(10) unsigned NOT NULL DEFAULT '0',
  `field_goals_attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `threepoint_goals_made` int(10) unsigned NOT NULL DEFAULT '0',
  `threepoint_goals_attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `freethrows_goals_made` int(10) unsigned NOT NULL DEFAULT '0',
  `freethrows_goals_attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `offence_rebounds` int(10) unsigned NOT NULL DEFAULT '0',
  `defense_rebounds` int(10) unsigned NOT NULL DEFAULT '0',
  `total_rebounds` int(10) unsigned NOT NULL DEFAULT '0',
  `assists` int(10) unsigned NOT NULL DEFAULT '0',
  `steals` int(10) unsigned NOT NULL DEFAULT '0',
  `blocks` int(10) unsigned NOT NULL DEFAULT '0',
  `turnovers` int(10) unsigned NOT NULL DEFAULT '0',
  `personal_fouls` int(10) unsigned NOT NULL DEFAULT '0',
  `plus_minus` int(10) unsigned NOT NULL DEFAULT '0',
  `points` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `game_team_stats`;
CREATE TABLE `game_team_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feed_id` int(10) unsigned NOT NULL,
  `team_id` int(10) unsigned NOT NULL,
  `q1` int(10) unsigned NOT NULL DEFAULT '0',
  `q2` int(10) unsigned NOT NULL DEFAULT '0',
  `q3` int(10) unsigned NOT NULL DEFAULT '0',
  `q4` int(10) unsigned NOT NULL DEFAULT '0',
  `ot` int(10) unsigned NOT NULL DEFAULT '0',
  `totalscore` int(10) unsigned NOT NULL DEFAULT '0',
  `field_goals_total` int(10) unsigned NOT NULL DEFAULT '0',
  `field_goals_attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `field_goals_pct` double unsigned NOT NULL DEFAULT '0',
  `threepoint_goals_total` int(10) unsigned NOT NULL DEFAULT '0',
  `threepoint_goals_attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `threepoint_goals_pct` double unsigned NOT NULL DEFAULT '0',
  `freethrows_goals_total` int(10) unsigned NOT NULL DEFAULT '0',
  `freethrows_goals_attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `freethrows_goals_pct` double unsigned NOT NULL DEFAULT '0',
  `rebounds_total` int(10) unsigned NOT NULL DEFAULT '0',
  `rebounds_offence` int(10) unsigned NOT NULL DEFAULT '0',
  `rebounds_defense` int(10) unsigned NOT NULL DEFAULT '0',
  `assists_total` int(10) unsigned NOT NULL DEFAULT '0',
  `steals_total` int(10) unsigned NOT NULL DEFAULT '0',
  `turnovers_total` int(10) unsigned NOT NULL DEFAULT '0',
  `personal_fouls_total` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `league`;
CREATE TABLE `league` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `season` char(9) NOT NULL DEFAULT '',
  `sport` enum('basketball','football') NOT NULL,
  `is_enabled` tinyint(3) unsigned NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feed_id` int(10) unsigned NOT NULL,
  `team_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `position` varchar(5) NOT NULL DEFAULT '',
  `age` int(5) unsigned NOT NULL,
  `heigth` varchar(100) NOT NULL DEFAULT '',
  `weigth` int(5) NOT NULL DEFAULT '0',
  `college` varchar(100) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `players_stats`;
CREATE TABLE `players_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` int(10) unsigned NOT NULL,
  `rank` int(10) unsigned NOT NULL,
  `games_played` int(10) unsigned NOT NULL DEFAULT '0',
  `games_started` int(10) unsigned NOT NULL DEFAULT '0',
  `minutes` double unsigned NOT NULL DEFAULT '0',
  `points_per_game` double unsigned NOT NULL DEFAULT '0',
  `offensive_rebounds_per_game` double unsigned NOT NULL DEFAULT '0',
  `defensive_rebounds_per_game` double unsigned NOT NULL DEFAULT '0',
  `rebounds_per_game` double unsigned NOT NULL DEFAULT '0',
  `assists_per_game` double unsigned NOT NULL DEFAULT '0',
  `steals_per_game` double unsigned NOT NULL DEFAULT '0',
  `blocks_per_game` double unsigned NOT NULL DEFAULT '0',
  `turnovers_per_game` double unsigned NOT NULL DEFAULT '0',
  `fouls_per_game` double unsigned NOT NULL DEFAULT '0',
  `efficiency_rating` double unsigned NOT NULL DEFAULT '0',
  `fg_made_per_game` double unsigned NOT NULL DEFAULT '0',
  `fg_attempts_per_game` double unsigned NOT NULL DEFAULT '0',
  `fg_pct` double unsigned NOT NULL DEFAULT '0',
  `three_point_made_per_game` double unsigned NOT NULL DEFAULT '0',
  `three_point_attempts_per_game` double unsigned NOT NULL DEFAULT '0',
  `three_point_pct` double unsigned NOT NULL DEFAULT '0',
  `free_throws_made_per_game` double unsigned NOT NULL DEFAULT '0',
  `free_throws_attempts_per_game` double unsigned NOT NULL DEFAULT '0',
  `free_throws_pct` double unsigned NOT NULL DEFAULT '0',
  `two_point_made_per_game` double unsigned NOT NULL DEFAULT '0',
  `two_point_attemps_per_game` double unsigned NOT NULL DEFAULT '0',
  `two_point_pct` double unsigned NOT NULL DEFAULT '0',
  `points_per_shot` double unsigned NOT NULL DEFAULT '0',
  `field_goal_pct_avg` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `player_points`;
CREATE TABLE `player_points` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `center_point` int(10) DEFAULT '0',
  `guard_point` int(10) DEFAULT '0',
  `forward_point` int(10) DEFAULT '0',
  `sort_order` int(10) DEFAULT '0',
  `enabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shedules`;
CREATE TABLE `shedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feed_id` int(10) unsigned NOT NULL,
  `hometeam` int(10) unsigned NOT NULL DEFAULT '0',
  `awayteam` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(100) NOT NULL DEFAULT '',
  `date_start` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `static_page`;
CREATE TABLE `static_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 - Enabled; 1 - Disabled',
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feed_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `nickname` varchar(50) NOT NULL DEFAULT '',
  `alias` varchar(30) NOT NULL,
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 - Not Active; 1 - Active',
  `role` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - User; 1 - Admin',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_oauth`;
CREATE TABLE `user_oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oauth_id` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - Facebook; 1 - Twitter',
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_id` (`oauth_id`),
  KEY `fk_user_oauth_user_id` (`user_id`),
  CONSTRAINT `fk_user_oauth_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2016-03-23 12:03:34