<?php

namespace tests\codeception\console\unit\models;

use Yii;
use tests\codeception\console\unit\DbTestCase;
use Codeception\Specify;
use tests\codeception\console\fixtures\UserFixture;
use common\models\Players;
use common\models\PlayersStats;
use common\models\GamePlayerStats;
use common\models\GameTeamStats;
use common\models\Team;
use common\models\Schedules;

/**
 * Login form test
 */
class XmlTest extends DbTestCase {

    protected $feedUrl = 'http://www.goalserve.com/getfeed/b517fbffb7a5404799504bd145bed582/bsktbl/{PATH}';
    protected $localFile = false;

    use Specify;

    public function testPlayerXml() {
        $query = Team::find();
        foreach ($query->each(50) as $modelTeam) {
            $url = "{$modelTeam->alias}_rosters";
            $xml = $this->loadXml($url);
            if (!$xml) {
                if ($this->savePlayers($modelTeam, $xml)) {
                    $this->assertTrue(true);
                } else {
                    $this->assertTrue(false);
                }
            }
        }
    }

    protected function savePlayers($modelTeam, $xml) {
        foreach ($xml->xpath('//player') as $player) {
            $playerDB = Players::findOne(['feed_id' => $player['id']]);

            $playerNew = $playerDB ? $playerDB : new Players;
            $playerNew->number = (int) $player['number'];
            $playerNew->team_id = $modelTeam->feed_id;
            $playerNew->feed_id = (int) $player['id'];
            $playerNew->name = (string) $player['name'];
            $playerNew->position = (string) $player['position'];
            $playerNew->age = (int) $player['age'];
            $playerNew->height = (string) $player['heigth'];
            $playerNew->weight = (int) $player['weigth'];
            $playerNew->college = (string) $player['college'];
            if ($playerNew->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function testPlayerStatsXml() {
        $query = Team::find();
        foreach ($query->each(50) as $modelTeam) {
            $url = "{$modelTeam->alias}_stats";
            $xml = $this->loadXml($url);
            if (!$xml) {
                if ($this->savePlayersStats($modelTeam, $xml)) {
                    $this->assertTrue(true);
                } else {
                    $this->assertTrue(false);
                }
            }
        }
    }

    public function savePlayersStats($modelTeam, $xml) {
        foreach ($xml->xpath('//player') as $player) {
            $statsDB = PlayersStats::findOne(['player_id' => $player['id']]);
            $statsNew = $statsDB ? $statsDB : new PlayersStats;
            $statsNew->player_id = (int) $player['id'];
            $statsNew->rank = (int) $player['rank'];
            if (isset($player['games_played'])) {
                $statsNew->games_played = (int) $player['games_played'];
                $statsNew->games_started = (int) $player['games_started'];
                $statsNew->minutes = (float) $player['minutes'];
                $statsNew->points_per_game = (float) $player['points_per_game'];
                $statsNew->offensive_rebounds_per_game = (float) $player['offensive_rebounds_per_game'];
                $statsNew->defensive_rebounds_per_game = (float) $player['defensive_rebounds_per_game'];
                $statsNew->rebounds_per_game = (float) $player['rebounds_per_game'];
                $statsNew->assists_per_game = (float) $player['assists_per_game'];
                $statsNew->blocks_per_game = (float) $player['blocks_per_game'];
                $statsNew->turnovers_per_game = (float) $player['turnovers_per_game'];
                $statsNew->fouls_per_game = (float) $player['fouls_per_game'];
                $statsNew->efficiency_rating = (float) $player['efficiency_rating'];
            }

            if (isset($player['fg_made_per_game'])) {
                $statsNew->fg_made_per_game = (int) $player['fg_made_per_game'];
                $statsNew->fg_attempts_per_game = (int) $player['fg_attempts_per_game'];
                $statsNew->fg_pct = (float) $player['fg_pct'];
                $statsNew->three_point_made_per_game = (float) $player['three_point_made_per_game'];
                $statsNew->three_point_attempts_per_game = (float) $player['three_point_attempts_per_game'];
                $statsNew->three_point_pct = (float) $player['three_point_pct'];
                $statsNew->free_throws_made_per_game = (float) $player['free_throws_made_per_game'];
                $statsNew->free_throws_attempts_per_game = (float) $player['free_throws_attempts_per_game'];
                $statsNew->free_throws_pct = (float) $player['free_throws_pct'];
                $statsNew->two_point_made_per_game = (float) $player['two_point_made_per_game'];
                $statsNew->two_point_attemps_per_game = (float) $player['two_point_attemps_per_game'];
                $statsNew->two_point_pct = (float) $player['two_point_pct'];
                $statsNew->two_point_attemps_per_game = (float) $player['points_per_shot'];
                $statsNew->two_point_pct = (float) $player['field_goal_pct_avg'];
            }

            if ($statsNew->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function testShedulesXml() {
        $url = "nba-shedule";
        $xml = $this->loadXml($url);
        if (!$xml) {
            if ($this->saveShedule($xml)) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false);
            }
        }
    }

    public function saveShedule($xml) {
        foreach ($xml->xpath('//match') as $match) {
            $shedulesDB = Schedules::findOne(['feed_id' => $match['id']]);
            $shedulesNew = $shedulesDB ? $shedulesDB : new Schedules;
            $shedulesNew->feed_id = (int) $match['id'];
            $shedulesNew->status = (string) $match['status'];
            $shedulesNew->hometeam = (int) $match->hometeam['id'];
            $shedulesNew->awayteam = (int) $match->awayteam['id'];
            $shedulesNew->date_start = date('Y-m-d H:i', strtotime($match['formatted_date'] . $match['time']));

            if ($shedulesNew->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function testGameTeamPlayerStatsXml($league = null) {
        echo 'loading game stats', PHP_EOL;
        $url = 'nba-scores.xml';
        $this->localFile = true;
        $xml = $this->loadXml($url);
        foreach ($xml->xpath('//match') as $match) {
            $check = true;
            $feed_id = (int)$match['id'];
            foreach ($match->hometeam as $hometeam){
                if(!$this->saveSingleGameTeamStats($feed_id,$hometeam,$match,'hometeam')){
                    $check = false;
                }
            }
            foreach ($match->awayteam as $awayteam){
                if(!$this->saveSingleGameTeamStats($feed_id,$awayteam,$match,'awayteam')){
                    $check = false;
                }
            }
            foreach ($match->player_stats->hometeam->starters->player as $player){
                if(!$this->saveSingleGamePlayerStats($feed_id,$player)){
                    $check = false;
                }
            }
            foreach ($match->player_stats->hometeam->bench->player as $player){
                if(!$this->saveSingleGamePlayerStats($feed_id,$player)){
                    $check = false;
                }
            }
        }
        $this->assertTrue($check);
    }
    
    private function saveSingleGameTeamStats($feed_id,$stats,$match,$location){
        $team_id = (int)$stats['id'];
        $gameTeamStatsDB = GameTeamStats::findOne(['feed_id' => $feed_id,'team_id'=>$team_id]);
        $gameTeamStatsNew = $gameTeamStatsDB? $gameTeamStatsDB :new GameTeamStats;
        $gameTeamStatsNew->feed_id = (int) $feed_id;
        $gameTeamStatsNew->team_id = (int) $team_id;
        $gameTeamStatsNew->q1 = (int) $stats['q1'];
        $gameTeamStatsNew->q2 = (int) $stats['q2'];
        $gameTeamStatsNew->q3 = (int) $stats['q3'];
        $gameTeamStatsNew->q4 = (int) $stats['q4'];
        $gameTeamStatsNew->ot = (int) $stats['ot'];
        $gameTeamStatsNew->totalscore = (int) $stats['totalscore'];
        $gameTeamStatsNew->field_goals_total = (int) $match->team_stats->{$location}->field_goals_made['total'];
        $gameTeamStatsNew->field_goals_attempts = (int) $match->team_stats->{$location}->field_goals_made['attempts'];
        $gameTeamStatsNew->field_goals_pct = (float) $match->team_stats->{$location}->field_goals_made['pct'];
        $gameTeamStatsNew->threepoint_goals_total = (int) $match->team_stats->{$location}->threepoint_goals_made['total'];
        $gameTeamStatsNew->threepoint_goals_attempts = (int) $match->team_stats->{$location}->threepoint_goals_made['attempts'];
        $gameTeamStatsNew->threepoint_goals_pct = (float) $match->team_stats->{$location}->threepoint_goals_made['pct'];
        $gameTeamStatsNew->freethrows_goals_total = (int) $match->team_stats->{$location}->freethrows_goals_made['total'];
        $gameTeamStatsNew->freethrows_goals_attempts = (int) $match->team_stats->{$location}->freethrows_goals_made['attempts'];
        $gameTeamStatsNew->freethrows_goals_pct = (float) $match->team_stats->{$location}->freethrows_goals_made['pct'];
        $gameTeamStatsNew->rebounds_total = (int) $match->team_stats->{$location}->rebounds['total'];
        $gameTeamStatsNew->rebounds_offense = (int) $match->team_stats->{$location}->rebounds['offense'];
        $gameTeamStatsNew->rebounds_defense = (int) $match->team_stats->{$location}->rebounds['defense'];
        $gameTeamStatsNew->assists_total = (int) $match->team_stats->{$location}->assists['total'];
        $gameTeamStatsNew->steals_total = (int) $match->team_stats->{$location}->steals['total'];
        $gameTeamStatsNew->turnovers_total = (int) $match->team_stats->{$location}->turnovers['total'];
        $gameTeamStatsNew->personal_fouls_total = (int) $match->team_stats->{$location}->personal_fouls['total'];
        if($gameTeamStatsNew->save()){
           return true;
        }else{
           return false;
        }
    }
        
    private function saveSingleGamePlayerStats($feed_id,$stats){
        $player_id = (int)$stats['id'];
        $gamePlayerStatsDB = GamePlayerStats::findOne(['feed_id' => $feed_id,'player_id'=>$player_id]);
        $gamePlayerStatsNew = $gamePlayerStatsDB? $gamePlayerStatsDB :new GamePlayerStats;
        $gamePlayerStatsNew->feed_id = (int) $feed_id;
        $gamePlayerStatsNew->player_id = (int) $player_id;
        $gamePlayerStatsNew->position = (string) $stats['pos'];
        $gamePlayerStatsNew->minutes = (int) $stats['minutes'];
        $gamePlayerStatsNew->field_goals_made = (int) $stats['field_goals_made'];
        $gamePlayerStatsNew->field_goals_attempts = (int) $stats['field_goals_attempts'];
        $gamePlayerStatsNew->threepoint_goals_made = (int) $stats['threepoint_goals_made'];
        $gamePlayerStatsNew->threepoint_goals_attempts = (int) $stats['threepoint_goals_attempts'];
        $gamePlayerStatsNew->freethrows_goals_made = (int) $stats['freethrows_goals_made'];
        $gamePlayerStatsNew->freethrows_goals_attempts = (int) $stats['freethrows_goals_attempts'];
        $gamePlayerStatsNew->offense_rebounds = (int) $stats['offense_rebounds'];
        $gamePlayerStatsNew->defense_rebounds = (int) $stats['defense_rebounds'];
        $gamePlayerStatsNew->total_rebounds = (int) $stats['total_rebounds'];
        $gamePlayerStatsNew->assists = (int) $stats['assists'];
        $gamePlayerStatsNew->steals = (int) $stats['steals'];
        $gamePlayerStatsNew->blocks = (int) $stats['blocks'];
        $gamePlayerStatsNew->turnovers = (int) $stats['turnovers'];
        $gamePlayerStatsNew->personal_fouls = (int) $stats['personal_fouls'];
        $gamePlayerStatsNew->plus_minus = (int) $stats['plus_minus'];
        $gamePlayerStatsNew->points = (int) $stats['points'];
        if($gamePlayerStatsNew->save()){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function testloadGameCommentsXml()
    {
        $url = 'nba-playbyplay.xml';
        $this->localFile=true;
        $xml =  $this->loadXml($url);
        if($xml){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
        
    }

    /**
     * @param $str
     * @return bool|\SimpleXMLElement
     */
    protected function stringToXml($str) {
        if (!$str) {
            return false;
        }
        $errorMode = libxml_use_internal_errors(true);
        $xml = simplexml_load_string($str);
        $errors = libxml_get_errors();
        libxml_clear_errors();
        libxml_use_internal_errors($errorMode);
        if ($errors) {
            print_r($errors);
            return false;
        }
        if ($resp = $xml->xpath('/error')) {
            print_r(trim((string) $resp[0]));
            return false;
        }
        return $xml;
    }

    /**
     * @param $url
     * @return bool|string
     */
    protected function get($url) {
        $client = new \GuzzleHttp\Client();
        try {
            $r = $client->get($url);
            if (200 == $r->getStatusCode()) {
                return (string) $r->getBody();
            }
            return false;
        } catch (\Exception $e) {
            \Yii::error($e->getMessage());
            return false;
        }
    }

    protected function loadXml($url) {
        $url = trim($url, '/');
        if ($this->localFile) {
            $str = @file_get_contents(\Yii::getAlias('@common/components/xml/') . $url);
        } else {
            $url = str_replace('{PATH}', $url, $this->feedUrl);
            $str = $this->get($url);
            $this->lastUrl = $url;
        }
        return $str ? $this->stringToXml($str) : false;
    }

}
