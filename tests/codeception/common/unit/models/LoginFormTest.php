<?php

namespace tests\codeception\common\unit\models;

use Yii;
use tests\codeception\common\unit\DbTestCase;
use Codeception\Specify;
use common\models\LoginForm;
use frontend\models\SignupForm;
use tests\codeception\common\fixtures\UserFixture;

/**
 * Login form test
 */
class LoginFormTest extends DbTestCase
{

    use Specify;

    public $login = 'Yaroslavka';
    public $password = '12345678';
    public $registerData = 
    ['SignupForm'=>[
       'username'=>'TestRegistration',
       'email'=>'Test@test.com',
       'password'=>'12345678',
       'first_name'=>'Test',
       'last_name'=>'Test2',
    ]];
    public $registerDataError = 
    ['SignupForm'=>[
       'username'=>'TestRegistration',
       'email'=>'TestRegistration',
       'password'=>'12345678',
       'first_name'=>'Test',
       'last_name'=>'Test2',
    ]];
    
    

    public function setUp()
    {
        parent::setUp();

        Yii::configure(Yii::$app, [
            'components' => [
                'user' => [
                    'class' => 'yii\web\User',
                    'identityClass' => 'common\models\User',
                ],
            ],
        ]);
    }

    protected function tearDown()
    {
        Yii::$app->user->logout();
        parent::tearDown();
    }

    public function testLoginUser()
    {
        $user = new LoginForm(['username' =>$this->login,'password' => $this->password]);
        $this->assertTrue($user->login());
    }

    public function testNoLoginUser()
    {
        $user = new LoginForm(['username' => 'Admiralcev@mail.ru','password' => '654321']);
        $this->assertFalse($user->login());
    }

    public function testRegistration()
    {
        $user = new SignupForm();
        $user->load($this->registerData);
        $user->signup();
        $this->assertInstanceOf('\frontend\models\SignupForm', $user, 'user should be valid');
        expect('username should be correct', $user->username)->equals($this->registerData['SignupForm']['username']);
        expect('email should be correct', $user->email)->equals($this->registerData['SignupForm']['email']);
        expect('password should be correct', $user->password)->equals($this->registerData['SignupForm']['password']);
    }
    
    public function testNoRegistrationBadEmail()
    {
        $user = new SignupForm();
        $user->load($this->registerDataError);
        $user->signup();
        expect('mot valid email', $user->signup())->null();
    }

    public function testNoRegistrationUnique()
    {
        $user = new SignupForm();
        $user->load($this->registerData);
        $user->signup();
        expect('username and email are in use, user should not be created', $user->signup())->null();
    }
    
    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => '@tests/codeception/common/unit/fixtures/data/models/user.php'
            ],
        ];
    }
}
