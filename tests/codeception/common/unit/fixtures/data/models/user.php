<?php

return [
    'user1'=>[
        'username' => 'Yaroslavka',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR',
        'password'=>'$2y$13$/u7.Dtr4CB/URrUuQmlcBuguI7ogGv6o0/yWSBtzWSzbUAIm/Iw6G',
        'password_reset_token' => 'ExzkCOaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312317',
        'first_name' => 'Yarolav',
        'last_name'=>'Volkodav',
        'status'=>1,
        'role'=>0,
        'created' => '1402312317',
        'updated' => '1402312317',
        'email' => 'Admiralcev@mail.ru',
    ],
];
