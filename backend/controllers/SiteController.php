<?php
namespace backend\controllers;

use Yii;
use frontend\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends BackendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['site/index']);
        } else {
            return $this->render('login', ['model' => $model]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->frontUrlManager->createUrl('site/index'));
    }
}
