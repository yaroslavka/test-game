<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name' => 'Discount UA API',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'homeUrl' => '/',
    'components' => [
        'request' => [
            'baseUrl' => '/admin'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'assetManager'=>[
		    'class'=>'yii\web\AssetManager',
		    'bundles'=>[
			    'insolita\wgadminlte\ExtAdminlteAsset'=>[
				    'depends'=>[
					    'yii\web\YiiAsset',
					    'dmstr\web\AdminLteAsset'
				    ]
			    ],
			    'insolita\wgadminlte\JCookieAsset'=>[
				    'sourcePath'=>'@bower/jquery-cookie',
				    'depends'=>[
					    'yii\web\YiiAsset',
					    'dmstr\web\AdminLteAsset'
				    ],
			    ],
		    ]
	    ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'static-pages/<id:\w+>' => 'static-pages/view',
                '<controller:\w+>s' => '<controller>/index',
                '<controller:\w+>/update/<id:\d+>' => '<controller>/update',
                '<controller:\w+>/delete/<id:\d+>' => '<controller>/delete',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
            ],
        ],
        'frontUrlManager' => require(__DIR__ . '/../../frontend/config/url_manager.php'),
    ],
    'params' => $params,
];
