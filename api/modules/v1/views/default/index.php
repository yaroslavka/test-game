<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<script>
    $(function(){
        var url = window.location.search.match(/url=([^&]+)/);
        if (url && url.length > 1) {
            url = decodeURIComponent(url[1]);
        } else {
            url = "<?php echo Url::to(['swagger-config'], true); ?>";
        }


        window.swaggerUi = new SwaggerUi({
            url: url,
            dom_id:"swagger-ui-container",
            validatorUrl: null
        });

        window.swaggerUi.load();

        /*window.authorizations.add("key", {name : "keyy", type: "header", value: "123456"});
         var obj = new ApiKeyAuthorization("api_key", '11', "header");
         console.log(obj);*/


        $('body').on('keyup, change', '#input_apiKey', function() {
            var key = $(this).val();
            window.authorizations.add("key", new ApiKeyAuthorization("apikey", key, "header"));
        });
    });
</script>

<div id="header">
    <div class="swagger-ui-wrap">
        <a id="logo" href="http://swagger.io">swagger</a>
        <form id="api_selector">
            <div class="input"><input placeholder="http://example.com/api" id="input_baseUrl" name="baseUrl" type="text"/></div>
            <div class="input"><input placeholder="api_key" id="input_apiKey" name="apiKey" type="text"/></div>
            <div class="input"><a id="explore" href="#" data-sw-translate>Explore</a></div>
        </form>
    </div>
</div>
<div id="message-bar" class="swagger-ui-wrap" data-sw-translate>&nbsp;</div>
<div id="swagger-ui-container" class="swagger-ui-wrap"></div>