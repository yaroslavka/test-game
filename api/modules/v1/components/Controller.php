<?php

namespace api\modules\v1\components;

use yii;
use yii\web\NotFoundHttpException;
use yii\rest\ActiveController;
use yii\web\Response;
use common\models\Licensees;


class Controller extends \yii\rest\Controller
{

    public $licenseesModel;

    public $apiResponse = [
        'status'=>200,
        'data'=>[],
        'errors'=>[]
    ];

	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
        \Yii::$app->response->format = Response::FORMAT_JSON;
		return true;
	}


    public function setResponseData($data)
    {
        $this->apiResponse['data'] = yii\helpers\ArrayHelper::merge($this->apiResponse['data'], $data);
    }

    public function sendApiResponse()
    {
        return $this->apiResponse;
    }

}