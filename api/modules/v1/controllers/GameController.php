<?php

namespace api\modules\v1\controllers;

use common\models\Category;
use common\models\Game;
use common\models\Stock;
use api\modules\v1\components\Controller;

class GameController extends Controller
{
	protected function verbs()
	{
		return [
			'index' => ['POST'],
		];
	}

	public function actionIndex()
	{
          $post = \Yii::$app->request->post('data');
          if($post){
              $game = new Game();
              $this->setResponseData(['result'=>$game->saveGame($post)]);
              return $this->sendApiResponse();
          }
	}
}


